FROM node:16 as base

WORKDIR /app

COPY ./ /app

RUN cd /app
RUN npm install

CMD ["ls"]

FROM node:16 as backend

WORKDIR /app

COPY --from=base /app /app

RUN cd /app/backend/
RUN cp /app/backend/.env.example /app/backend/.env

EXPOSE 3000

CMD ["npm", "start", "-w", "backend"]

FROM node:16 as frontend

WORKDIR /app

COPY --from=base /app /app

EXPOSE 4200

CMD ["npm", "start", "-w", "frontend", "--", "--host", "0.0.0.0"]
