# Justo Chanllenge (Spy agency)

This challenge was maded with:

- MariaDB: For database.
- NestJS: For backend.
- MikrORM: For database ORM.
- Angular: For frontend.
- Bulma: For CSS style.

## Requeriments

- Nodejs and npm (the latest version as posible).
- MariaDB (optional).
- Docker and docker-compose (optional).

## Installation

This project is using a npm monorepo. You only need to run `npm install` one time in this folder.

### Start the project (simple method, docker-compose required)

The project as a dockerbuild and docker-compose.yml to setup and run the project.

1. Run the command:
```
$ npm run all
or
$ docker-compose up
```

2. In other terminal, run the command:
```
$ npm run migrations
or
$ docker-compose up migrator
```
Open the application at http://localhost:4200

### Start the project (manual way, MariaDB service and database preconfigurated required)

1. Run `npm install` in root folder
2. Setup the backend environment on the file `backend/.env`. You have an example at `backend/.env.example`.
3. Run migrations with `$ npm run migration:up -w backend`.
4. Run the backend with `$ npm run start -w backend`.
5. In other terminal. Run the frontend with `$ npm run start -w frontend`.
6. Open the application at http://localhost:4200

## Users

All the password's are `test`.

| User | Email |
| ---- | ----- |
| Big boss | boss@mail.com |
| Manager 0 | manager0@mail.com |
| Manager 1 | manager1@mail.com |
| Manager 2 | manager2@mail.com |
| Hitman 0 | hitman0@mail.com |
| Hitman x | hitmanx@mail.com |
| Hitman 9 | hitman0@mail.com |

