import {
  Collection,
  Entity,
  Enum,
  ManyToOne,
  OneToMany,
  PrimaryKey,
  Property,
} from '@mikro-orm/core';

export enum UserTypeEnum {
  'hitman',
  'manager',
  // 'boss',
}

@Entity()
export default class User {
  @PrimaryKey()
  id: number;

  @Property({ unique: true })
  email: string;

  @Property()
  password: string;

  @Property()
  name: string;

  @Property({ nullable: true })
  description: string;

  @Property({ default: true })
  active = true;

  @Enum(() => UserTypeEnum)
  userType: UserTypeEnum = UserTypeEnum.hitman;

  @ManyToOne(() => User, { nullable: true })
  manager: User;

  @OneToMany(() => User, (user) => user.manager)
  lackers = new Collection<User>(this);
}
