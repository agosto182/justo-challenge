import { EntityRepository, FilterQuery } from '@mikro-orm/core';
import { InjectRepository } from '@mikro-orm/nestjs';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import User, { UserTypeEnum } from './entities/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  private readonly salt = 11;

  constructor(
    @InjectRepository(User)
    private userRepository: EntityRepository<User>,
  ) {}

  async create(userInput: CreateUserDto) {
    const { password, email } = userInput;
    const userExists = await this.userRepository.findOne({ email });
    if(userExists) {
      throw new BadRequestException('Email already exists on database');
    }
    const finalPassword = await bcrypt.hash(password, this.salt);
    const user = this.userRepository.create({
      ...userInput,
      password: finalPassword,
    });
    const manager = await this.findManager();
    user.manager = manager;

    await this.userRepository.persistAndFlush(user);

    const userCreated = await this.findOne(user.id);

    return userCreated;
  }

  async update(id: number, userInput: UpdateUserDto) {
    const user = { ...userInput };
    if (userInput.password) {
      user.password = await bcrypt.hash(userInput.password, this.salt);
    }
    await this.userRepository.nativeUpdate({ id }, user);
    const userUpdated = await this.findOne(id);

    return userUpdated;
  }

  findAll(where: FilterQuery<User>) {
    return this.userRepository.find(
      where,
      { populate: ['manager', 'lackers'] },
    );
  }

  async findOne(id: number) {
    const user = await this.userRepository.findOne(
      { id },
      { populate: ['manager', 'lackers'] },
    );
    return user;
  }

  async inactive(id: number) {
    const user = await this.userRepository.findOne({ id });
    user.active = false;

    this.userRepository.flush();

    return true;
  }

  async addLacker(id: number, lackerId: number) {
    const user = await this.userRepository.findOne({ id });
    const lackerRef = this.userRepository.getReference(lackerId);
    user.lackers.add(lackerRef);
    // The user change to manager automatic
    user.userType = UserTypeEnum.manager;

    this.userRepository.flush();

    return true;
  }

  async removeLacker(id: number, lackerId: number) {
    const user = await this.userRepository.findOne(
      { id },
      { populate: ['lackers'] },
    );
    const lackerRef = this.userRepository.getReference(lackerId);
    user.lackers.remove(lackerRef);

    this.userRepository.flush();

    return true;
  }

  async findByEmail(email: string) {
    const user = await this.userRepository.findOne({
      email,
      active: true,
    });
    return user;
  }

  private async findManager() {
    const managers = await this.userRepository.find({userType: UserTypeEnum.manager }, {populate: ['lackers']});
    const managerWithLessLackears = managers.reduce((first, second) => {
      return first.lackers.length > second.lackers.length ? second : first;
    });
    return managerWithLessLackears;
  }
}
