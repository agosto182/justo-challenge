import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  SetMetadata,
  Put,
  UnauthorizedException,
  Query,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthGuard } from 'src/auth/auth.guard';
import { ADMIN_REQUIRED } from 'src/constants';
import { AuthUser } from 'src/auth/auth.decorator';
import User from './entities/user.entity';
import { FilterQuery } from '@mikro-orm/core';

@Controller('users')
@UseGuards(AuthGuard)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  @SetMetadata(ADMIN_REQUIRED, true)
  findAll(
    @AuthUser()
    user: User,
    @Query('onlyActive')
    onlyActive: boolean = false
  ) {
    const where: FilterQuery<User> = { 
      id: { $ne: 1 }
    };
    if (user.id !== 1) {
      where.manager = user.id;
    }
    if(onlyActive) {
      where.active = true;
    }
    return this.usersService.findAll(where);
  }

  @Get(':id')
  @SetMetadata(ADMIN_REQUIRED, true)
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }

  @Patch(':id')
  @SetMetadata(ADMIN_REQUIRED, true)
  disableUser(@Param('id') id: string) {
    return this.usersService.inactive(+id);
  }

  @Put('lacker')
  @SetMetadata(ADMIN_REQUIRED, true)
  async putLacker(
    @Body('lackerId')
    lackerId: number,
    @Body('managerId')
    managerId: number,
    @AuthUser()
    user: User,
  ) {
    if (user.id != 1) {
      throw new UnauthorizedException();
    }
    const res = await this.usersService.addLacker(managerId, lackerId);
    return res;
  }

  @Delete('lacker')
  @SetMetadata(ADMIN_REQUIRED, true)
  async deleteLacker(
    @Query('lackerId')
    lackerId: number,
    @Query('managerId')
    managerId: number,
    @AuthUser()
    user: User,
  ) {
    if (user.id != 1) {
      throw new UnauthorizedException();
    }
    const res = await this.usersService.removeLacker(managerId, lackerId);
    return res;
  }
}
