import { BadRequestException, Body, Controller, Post, UnauthorizedException } from '@nestjs/common';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private userService: UsersService,
  ) {}

  @Post('/login')
  async login(
    @Body('email')
    email: string,
    @Body('password')
    password: string,
  ) {
    const auth = await this.authService.tryLoggin(email, password);

    if (!auth) {
      throw new BadRequestException('Invalid credentials');
    }
    const token = this.authService.createToken(auth);

    return { token, user: auth };
  }

  @Post('/register')
  async register(
    @Body()
    userInput: CreateUserDto,
  ) {
    const user = await this.userService.create(userInput);

    const token = this.authService.createToken(user);

    return { token, user };
  }
}
