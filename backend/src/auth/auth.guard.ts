import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ADMIN_REQUIRED } from 'src/constants';
import { UserTypeEnum } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private authService: AuthService,
    private userService: UsersService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const managerOrBossRequired = this.reflector.get<string[]>(
      ADMIN_REQUIRED,
      context.getHandler(),
    );
    const req = context.switchToHttp().getRequest();
    const { token } = req.headers;
    let isValid = false;

    try {
      isValid = await this.authService.verifyToken(token);
      const user = this.authService.parseToken(token);

      if (
        managerOrBossRequired &&
        user['userType'] !== UserTypeEnum.manager &&
        +user['id'] !== 1
      ) {
        console.warn('User unauthorized');
        throw new UnauthorizedException();
      }
      req.user = await this.userService.findOne(user['id']);
    } catch (err) {
      console.warn(err);
      throw new UnauthorizedException();
    }

    return isValid;
  }
}
