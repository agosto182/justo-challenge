import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt';
import User from 'src/users/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  secret = this.configService.get('JWT_SECRET');

  constructor(
    private userService: UsersService,
    private jwt: JwtService,
    private configService: ConfigService,
  ) {}

  verifyToken(jwt: string) {
    return this.jwt.verify(jwt, {
      secret: this.secret,
    });
  }

  parseToken(token: string) {
    return this.jwt.decode(token);
  }

  createToken(user: User) {
    return this.jwt.sign(
      {
        id: user.id,
        email: user.email,
        name: user.name,
        userType: user.userType,
      },
      {
        secret: this.secret,
      },
    );
  }

  async tryLoggin(email: string, password: string) {
    let success = false;
    const user = await this.userService.findByEmail(email);

    if (user) {
      if (bcrypt.compareSync(password, user.password)) {
        success = true;
      }
    }

    return success && user;
  }
}
