import { HitStateEnum } from '../entities/hit.entity';

export class FindHitDto {
  id: number;

  description: string;

  targetName: string;

  state: HitStateEnum;

  assignee: number;

  createdBy: number;
}
