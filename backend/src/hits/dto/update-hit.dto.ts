import { Enum } from '@mikro-orm/core';
import { IsOptional } from 'class-validator';
import { HitStateEnum } from '../entities/hit.entity';

export class UpdateHitDto {
  @IsOptional()
  assignee: number;

  @IsOptional()
  @Enum(() => HitStateEnum)
  state: HitStateEnum;
}
