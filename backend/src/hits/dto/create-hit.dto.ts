import { IsEnum, IsNotEmpty } from 'class-validator';
import { HitStateEnum } from '../entities/hit.entity';

export class CreateHitDto {
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  targetName: string;

  @IsNotEmpty()
  assignee: number;

  @IsNotEmpty()
  state: HitStateEnum;
  createdBy?: number;
}
