import { Module } from '@nestjs/common';
import { HitsService } from './hits.service';
import { HitsController } from './hits.controller';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import Hit from './entities/hit.entity';

@Module({
  imports: [MikroOrmModule.forFeature([Hit])],
  controllers: [HitsController],
  providers: [HitsService],
})
export class HitsModule {}
