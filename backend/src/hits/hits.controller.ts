import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  BadRequestException,
  Query,
} from '@nestjs/common';
import { HitsService } from './hits.service';
import { CreateHitDto } from './dto/create-hit.dto';
import { UpdateHitDto } from './dto/update-hit.dto';
import { AuthUser } from 'src/auth/auth.decorator';
import User from 'src/users/entities/user.entity';
import { AuthGuard } from 'src/auth/auth.guard';

@Controller('hits')
@UseGuards(AuthGuard)
export class HitsController {
  constructor(private readonly hitsService: HitsService) {}

  @Post()
  create(
    @Body() createHitDto: CreateHitDto,
    @AuthUser()
    user: User,
  ) {
    return this.hitsService.create({
      ...createHitDto,
      createdBy: user.id,
    });
  }

  @Get()
  findAll(
    @AuthUser()
    user: User,
    @Query('onlyOpen')
    onlyOpenString: string
  ) {
    const onlyOpen = onlyOpenString == "true";
    const lackers = user.lackers.toArray().map(l => +l.id);
    return this.hitsService.findMyHits(user.id, lackers, onlyOpen);
  }

  @Get(':id')
  async findOne(
    @Param('id') id: string,
    @AuthUser()
    user: User
  ) {
    const lackers = user.lackers.toArray().map(l => +l.id);
    const hit = await this.hitsService.findOne(+id, user.id, lackers);
    if(!hit) {
      throw new BadRequestException('Hit not found');
    }
    return hit;
  }

  @Patch(':id')
  update(
    @Param('id') id: string, 
    @Body() updateHitDto: UpdateHitDto,
    @AuthUser()
    user: User
  ) {
    const lackers = user.lackers.toArray().map(l => +l.id);
    return this.hitsService.update(+id, updateHitDto, user.id, lackers);
  }
}
