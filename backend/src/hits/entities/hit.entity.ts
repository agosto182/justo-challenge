import { Entity, Enum, ManyToOne, PrimaryKey, Property } from '@mikro-orm/core';
import User from 'src/users/entities/user.entity';

export enum HitStateEnum {
  'Assigned',
  'Failed',
  'Completed',
}

@Entity()
export default class Hit {
  @PrimaryKey()
  id: number;

  @Property()
  description: string;

  @Property()
  targetName: string;

  @Enum(() => HitStateEnum)
  state: HitStateEnum = HitStateEnum.Assigned;

  @ManyToOne(() => User)
  assignee: User;

  @ManyToOne(() => User)
  createdBy: User;
}
