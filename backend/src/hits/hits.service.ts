import { EntityRepository, FilterQuery } from '@mikro-orm/core';
import { InjectRepository } from '@mikro-orm/nestjs';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateHitDto } from './dto/create-hit.dto';
import { UpdateHitDto } from './dto/update-hit.dto';
import Hit, { HitStateEnum } from './entities/hit.entity';

@Injectable()
export class HitsService {
  constructor(
    @InjectRepository(Hit)
    private hitRepository: EntityRepository<Hit>,
  ) {}

  async create(createHitDto: CreateHitDto) {
    const hit = this.hitRepository.create(createHitDto);
    await this.hitRepository.persistAndFlush(hit);
    return hit;
  }

  async update(id: number, updateHitDto: UpdateHitDto, userId: number, lackers = []) {
    const actualHitState = await this.findOne(id, userId, lackers);

    if (!actualHitState) {
      throw new BadRequestException('Hit does not exists');
    }

    if(actualHitState.state != HitStateEnum.Assigned) {
      throw new BadRequestException('Hit can not be updated');
    }

    await this.hitRepository.nativeUpdate({ id }, updateHitDto);

    const hit = await this.findOne(id, userId, lackers);
    return hit;
  }

  findMyHits(userId: number, lackers = [], onlyOpen: boolean = false) {
    const where = userId == 1 ? {} : {
      $or: [
        { assignee: +userId },  
        { assignee: { $in: lackers } }
      ],
    };
    if(onlyOpen) {
      where['state'] = HitStateEnum.Assigned;
    }
    return this.findAll(where);
  }

  async findOne(id: number, userId: number, lackers = []) {
    let filter = {};
    if(userId != 1) {
      filter = { 
        $or: [
          { assignee: userId },
          { assignee: { $in: lackers } }
        ]
      }
    }
    const hit = await this.hitRepository.findOne(
      { 
        $and: [
          { id },
          filter
        ]
      },
      { populate: ['assignee', 'createdBy'] },
    );
    return hit;
  }

  async findAll(where: FilterQuery<Hit>) {
    const hits = await this.hitRepository.find(where, {
      populate: ['assignee', 'createdBy'],
    });

    return hits;
  }

  remove(id: number) {
    return `This action removes a #${id} hit`;
  }
}
