import { Migration } from '@mikro-orm/migrations';

export class Migration20221219181304 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `user` (`id` int unsigned not null auto_increment primary key, `email` varchar(255) not null, `password` varchar(255) not null, `name` varchar(255) not null, `description` varchar(255) null, `active` tinyint(1) not null default true, `user_type` tinyint not null default 0, `manager_id` int unsigned null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `user` add unique `user_email_unique`(`email`);');
    this.addSql('alter table `user` add index `user_manager_id_index`(`manager_id`);');

    this.addSql('alter table `user` add constraint `user_manager_id_foreign` foreign key (`manager_id`) references `user` (`id`) on update cascade on delete set null;');
  }

}
