import { Migration } from '@mikro-orm/migrations';

export class Migration20221219191924 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `hit` (`id` int unsigned not null auto_increment primary key, `description` varchar(255) not null, `target_name` varchar(255) not null, `state` tinyint not null default 0, `assignee_id` int unsigned not null, `created_by_id` int unsigned not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `hit` add index `hit_assignee_id_index`(`assignee_id`);');
    this.addSql('alter table `hit` add index `hit_created_by_id_index`(`created_by_id`);');

    this.addSql('alter table `hit` add constraint `hit_assignee_id_foreign` foreign key (`assignee_id`) references `user` (`id`) on update cascade;');
    this.addSql('alter table `hit` add constraint `hit_created_by_id_foreign` foreign key (`created_by_id`) references `user` (`id`) on update cascade;');
  }

  async down(): Promise<void> {
    this.addSql('drop table if exists `hit`;');
  }

}
