import { Migration } from '@mikro-orm/migrations';
import * as bcrypt from 'bcrypt';

export class Migration20221220012604 extends Migration {

  async up(): Promise<void> {
    const password = bcrypt.hashSync('test', 10);
    this.addSql(`INSERT INTO user(email, name, password) VALUES ("boss@mail.com", "Big Boss", "${password}")`);
    for(let x = 0; x < 3; x++) {
      this.addSql(`INSERT INTO user(email, name, password, user_type) VALUES ("manager${x}@mail.com", "Manager ${x}", "${password}", 1)`);
    }

    for(let x = 0; x < 9; x++) {
      this.addSql(`INSERT INTO user(email, name, password, user_type, manager_id) VALUES ("hitman${x}@mail.com", "Hitman ${x}", "${password}", 0, 2)`);
    }
  }

}
