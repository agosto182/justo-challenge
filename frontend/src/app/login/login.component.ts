import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  badLogin = false;
  loginForm;

  constructor(
    private appService: AppService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    })
  }

  loginSubmit(credentials: any) {
    this.appService.tryLogin(credentials).subscribe(
      res => {
        this.appService.setAuth(res);
        this.router.navigate(['hits']);
      },
      err => {
        console.warn(err);
        this.badLogin = true;

        setTimeout(() => this.badLogin = false, 5000);
      },
      () => {
        this.loginForm.reset();
      }
    );
  }
}
