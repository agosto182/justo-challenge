import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HitsCreateComponent } from './hits-create.component';

describe('HitsCreateComponent', () => {
  let component: HitsCreateComponent;
  let fixture: ComponentFixture<HitsCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HitsCreateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HitsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
