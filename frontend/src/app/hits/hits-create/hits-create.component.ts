import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { toast } from 'bulma-toast';
import { HitsService } from '../hits.service';

@Component({
  selector: 'app-hits-create',
  templateUrl: './hits-create.component.html',
  styleUrls: ['./hits-create.component.css']
})
export class HitsCreateComponent {
  hitmans: any[] = [];
  status = [
    "Assigne",
    "Failed",
    "Completed"
  ];
  hitForm;
  messages = [];
  badHit = false;

  constructor(
    private hitsService: HitsService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.hitForm = this.formBuilder.group({
      targetName: '',
      description: '',
      assignee: '',
      state: ''
    });
    this.getHitmans();
  }

  getHitmans() {
    this.hitsService.getUsers(true).subscribe((res: any) => {
      this.hitmans = res;
    });
  }

  createHit(data: any) {
    console.log('data', data);
    this.hitsService.createHit(data).subscribe(
      (res: any) => {
        toast({
          message: 'Hit was created with success',
          position: "bottom-center",
          type: 'is-success'
        });
        this.router.navigate(['/hits']);
      },
      (err) => {
        this.badHit = true;
        this.messages = err.error.message;
      }
    );
  }
}
