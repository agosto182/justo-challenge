import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HitsBulkComponent } from './hits-bulk.component';

describe('HitsBulkComponent', () => {
  let component: HitsBulkComponent;
  let fixture: ComponentFixture<HitsBulkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HitsBulkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HitsBulkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
