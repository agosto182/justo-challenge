import { Component } from '@angular/core';
import { toast } from 'bulma-toast';
import { AppService } from 'src/app/app.service';
import { HitsService } from '../hits.service';

@Component({
  selector: 'app-hits-bulk',
  templateUrl: './hits-bulk.component.html',
  styleUrls: ['./hits-bulk.component.css']
})
export class HitsBulkComponent {
  hits: any[] = [];
  readonly states = [
    'Assignee',
    'Failed',
    'Completed'
  ];
  readonly stateClass = [
    'is-info',
    'is-danger',
    'is-success'
  ];
  isManager = this.appService.isManager();
  hitmen: any[] = [];

  constructor(
    private hitsService: HitsService,
    private appService: AppService
  ) {
    this.getHits();
    this.getHitmen();
  }

  getHits() {
    this.hitsService.getHits(true).subscribe((res: any) => {
      this.hits = res;
    });
  }

  getHitmen() {
    this.hitsService.getUsers(true).subscribe((res: any) => {
      this.hitmen = res;
    });
  }

  updateHit(hitId: number, event: any) {
    const assigneeId = +event.target.value;
    this.hitsService.updateHit(hitId, { assignee: assigneeId }).subscribe(res => {
      toast({
        message: "Hit assignee updated",
        position: 'bottom-center',
        type: 'is-success'
      });
    })
  }
}
