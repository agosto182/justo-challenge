import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { toast } from 'bulma-toast';
import { AppService } from 'src/app/app.service';
import { HitsService } from '../hits.service';

@Component({
  selector: 'app-hits-detail',
  templateUrl: './hits-detail.component.html',
  styleUrls: ['./hits-detail.component.css']
})
export class HitsDetailComponent implements OnInit {
  hit: any = {
    targetName: '',
    description: '',
    assignee: '',
    status: ''
  };
  hitmans: any[] = [];
  status = [
    "Assigne",
    "Failed",
    "Completed"
  ];
  hitForm;
  messages = [];
  badHit = false;
  hitId: number = 0;
  isManager = false;

  constructor(
    private hitsService: HitsService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private appService: AppService
  ) {
    this.hitForm = this.formBuilder.group({
      assignee: '',
      state: ''
    });
    this.isManager = this.appService.isManager();
    if(this.isManager) {
      this.getHitmans();
    }
  }

  ngOnInit() {
    const hitId = this.route.snapshot.params['id'];
    this.getHit(hitId);
    this.hitId = hitId;
  }

  getHitmans() {
    this.hitsService.getUsers(true).subscribe((res: any) => {
      this.hitmans = res;
    });
  }

  updateHit(data: any) {
    this.hitsService.updateHit(this.hitId, data).subscribe(
      (res: any) => {
        toast({
          message: 'Hit was updated with success',
          position: "bottom-center",
          type: 'is-success'
        });
      },
      (err) => {
        this.badHit = true;
        this.messages = err.error.message;
      },
      () => {
        this.getHit(this.hitId);
      }
    );
  }

  getHit(hitId: number) {
    this.hitsService.getHitDetail(hitId).subscribe(
      res => {
        this.hit = res;
        this.hitForm.setValue({
          assignee: this.hit.assignee.id,
          state: this.hit.state
        });
      },
      err => {
        console.warn(err);
        toast({
          message: err.error.message,
          type: 'is-danger',
          position: 'bottom-center'
        });
        this.router.navigate(['/hits']);
      }
    )
  }
}
