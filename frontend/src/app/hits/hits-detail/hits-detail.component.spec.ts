import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HitsDetailComponent } from './hits-detail.component';

describe('HitsDetailComponent', () => {
  let component: HitsDetailComponent;
  let fixture: ComponentFixture<HitsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HitsDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HitsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
