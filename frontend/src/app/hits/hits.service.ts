import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class HitsService {
  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) {}

  getHits(onlyOpen: boolean = false) {
    return this.http.get(`${this.apiUrl}/hits`, { params: { onlyOpen }});
  }

  getUsers(onlyActive: boolean = false) {
    return this.http.get(`${this.apiUrl}/users`, { 
      params: {
        onlyActive
      }
    });
  }

  createHit(data: any) {
    return this.http.post(`${this.apiUrl}/hits`, data);
  }

  updateHit(id:number, data: any) {
    return this.http.patch(`${this.apiUrl}/hits/${id}`, data);
  }

  getHitDetail(id: number) {
    return this.http.get(`${this.apiUrl}/hits/${id}`);
  }
}

