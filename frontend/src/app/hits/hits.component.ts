import { Component } from '@angular/core';
import { AppService } from '../app.service';
import { HitsService } from './hits.service';

@Component({
  selector: 'app-hits',
  templateUrl: './hits.component.html',
  styleUrls: ['./hits.component.css']
})
export class HitsComponent {
  hits: any[] = [];
  readonly states = [
    'Assignee',
    'Failed',
    'Completed'
  ];
  readonly stateClass = [
    'is-info',
    'is-danger',
    'is-success'
  ];
  isManager = this.appService.isManager();

  constructor(
    private hitsService: HitsService,
    private appService: AppService
  ) {
    this.getHits();
  }

  getHits() {
    this.hitsService.getHits().subscribe((res: any) => {
      this.hits = res;
    });
  }
}
