import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';
import { AppService } from './app.service';
import { Router } from '@angular/router';
import { toast } from 'bulma-toast';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private appService: AppService,
    private router: Router
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let req = request;
    if(this.appService.isLogged()) {
      req = req.clone({
        setHeaders: {
          token: this.appService.token || ''
        }
      })
    }
    return next.handle(req).pipe(
      catchError(err => {
        if(err.error.statusCode === 401) {
          toast({
            message: 'Your session was expired or you are not authorized',
            position: 'bottom-center',
            type: "is-warning"
          })
          this.router.navigate(['/logout']);
        }

        return throwError(err);
      })
    );
  }
}
