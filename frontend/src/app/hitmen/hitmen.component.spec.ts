import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HitmenComponent } from './hitmen.component';

describe('HitmenComponent', () => {
  let component: HitmenComponent;
  let fixture: ComponentFixture<HitmenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HitmenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HitmenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
