import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HitmenDetailComponent } from './hitmen-detail.component';

describe('HitmenDetailComponent', () => {
  let component: HitmenDetailComponent;
  let fixture: ComponentFixture<HitmenDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HitmenDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HitmenDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
