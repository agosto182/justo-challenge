import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { toast } from 'bulma-toast';
import { AppService } from 'src/app/app.service';
import { HitmenService } from '../hitmen.service';

@Component({
  selector: 'app-hitmen-detail',
  templateUrl: './hitmen-detail.component.html',
  styleUrls: ['./hitmen-detail.component.css']
})
export class HitmenDetailComponent implements OnInit {
  badHitman = false;
  messages = [];
  hitman: any = {
    id: 0,
    name: '',
    email: '',
    description: '',
    active: false,
    lackers: []
  }
  hitmen: any[] = [];
  hitmanForm;
  isTheBoss = false;

  constructor(
    private appService: AppService,
    private hitmenService: HitmenService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.hitmanForm = this.formBuilder.group({
      active: false,
      lackers: []
    });
  }
  
  ngOnInit() {
    const userId = this.route.snapshot.params['id'];
    this.getHitman(userId);

    this.isTheBoss = this.appService.user['id'] == 1;
    if(this.isTheBoss) {
      this.getHitmen();
    }
  }

  getHitmen() {
    this.hitmenService.getHitmens().subscribe((res: any) => {
      this.hitmen = res;
    });
  }

  updateHitman(data: any) {}

  getHitman(id: number) {
    this.hitmenService.getHitman(id).subscribe(
      (res: any) => {
        this.hitman = res;
        this.hitmanForm.setValue({
          active: res.active,
          lackers: res.lackers
        });
      },
      err => {
        console.warn(err);
        toast({
          message: 'User not found',
          type: 'is-warning',
          position: 'bottom-center'
        });
      }
    );
  }

  addLacker(event: any) {
    const { id: managerId } = this.hitman;
    const lackerId = event.target.value;
    this.hitmenService.putLacker(managerId, lackerId).subscribe(
      res => {
        toast({
          message: 'Lacker added',
          type: 'is-success',
          position: 'bottom-center'
        });
        this.getHitman(managerId);
      }
    );
  }

  removeLacker(lackerId: number) {
    const { id: managerId } = this.hitman;
    this.hitmenService.deleteLacker(managerId, lackerId).subscribe(
      res => {
        toast({
          message: 'Lacker removed',
          type: 'is-success',
          position: 'bottom-center'
        });
        this.getHitman(managerId);
      }
    );
  }

  disableUser() {
    if(confirm('Do you want to disable this hitman? This action cant be reverted.')){
      const { id: managerId } = this.hitman;
      this.hitmenService.disableUser(managerId).subscribe(
        res => {
          toast({
            message: 'Hitman was disabled',
            type: 'is-success',
            position: 'bottom-center'
          });

          this.getHitman(managerId);
        }
      )
    }
  }
}
