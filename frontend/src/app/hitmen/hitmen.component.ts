import { Component } from '@angular/core';
import { HitmenService } from './hitmen.service';

@Component({
  selector: 'app-hitmen',
  templateUrl: './hitmen.component.html',
  styleUrls: ['./hitmen.component.css']
})
export class HitmenComponent {
  hitmen: any[] = [];

  constructor(
    private hitmenService: HitmenService
  ) {
    this.getHitmen();
  }

  getHitmen() {
    this.hitmenService.getHitmens().subscribe((res: any) => {
      this.hitmen = res;
    });
  }
}
