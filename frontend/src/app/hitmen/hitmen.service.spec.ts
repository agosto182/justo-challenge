import { TestBed } from '@angular/core/testing';

import { HitmenService } from './hitmen.service';

describe('HitmenService', () => {
  let service: HitmenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HitmenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
