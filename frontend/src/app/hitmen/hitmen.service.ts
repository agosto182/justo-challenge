import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HitmenService {
  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  getHitmens() {
    return this.http.get(`${this.apiUrl}/users`);
  }

  createHitmen(data: any) {
    return this.http.post(`${this.apiUrl}/users`, data);
  }

  updateHitmen(id:number, data: any) {
    return this.http.patch(`${this.apiUrl}/users/${id}`, data);
  }

  getHitman(id: number) {
    return this.http.get(`${this.apiUrl}/users/${id}`);
  }

  putLacker(managerId: number, lackerId: number) {
    return this.http.put(`${this.apiUrl}/users/lacker`, {
      managerId,
      lackerId
    });
  }

  deleteLacker(managerId: number, lackerId: number) {
    return this.http.delete(`${this.apiUrl}/users/lacker`, {
      params: {
        managerId,
        lackerId
      }
    });
  }

  disableUser(id: number) {
    return this.http.patch(`${this.apiUrl}/users/${id}`, {});
  }
}
