import { Component } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent {
  user = {
    name: ''
  };
  isManager;

  constructor(
    private appService: AppService
  ) {
    this.user = this.appService.user;
    this.isManager = this.appService.isManager();
  }
}
