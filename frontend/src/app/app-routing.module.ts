import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HitmenDetailComponent } from './hitmen/hitmen-detail/hitmen-detail.component';
import { HitmenComponent } from './hitmen/hitmen.component';
import { HitsBulkComponent } from './hits/hits-bulk/hits-bulk.component';
import { HitsCreateComponent } from './hits/hits-create/hits-create.component';
import { HitsDetailComponent } from './hits/hits-detail/hits-detail.component';
import { HitsComponent } from './hits/hits.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { TemplateComponent } from './template/template.component';

const routes: Routes = [
  {
    path: 'register',
    component: RegisterComponent
  },
  { 
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: '',
    component: LoginComponent
  },
  { 
    path: '',
    component: TemplateComponent,
    children: [
      {
        path: 'hits',
        component: HitsComponent
      },
      {
        path: 'hits/create',
        component: HitsCreateComponent 
      },
      {
        path: 'hits/bulk',
        component: HitsBulkComponent 
      },

      {
        path: 'hits/:id',
        component: HitsDetailComponent
      },
      {
        path: 'hitmen',
        component: HitmenComponent
      },
      {
        path: 'hitmen/:id',
        component: HitmenDetailComponent 
      }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
