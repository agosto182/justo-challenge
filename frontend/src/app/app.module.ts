import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { TemplateComponent } from './template/template.component';
import { HitsComponent } from './hits/hits.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppService } from './app.service';
import { HitsService } from './hits/hits.service';
import { AuthInterceptor } from './auth.interceptor';
import { HitsCreateComponent } from './hits/hits-create/hits-create.component';
import { HitsDetailComponent } from './hits/hits-detail/hits-detail.component';
import { HitmenComponent } from './hitmen/hitmen.component';
import { HitmenDetailComponent } from './hitmen/hitmen-detail/hitmen-detail.component';
import { HitsBulkComponent } from './hits/hits-bulk/hits-bulk.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    TemplateComponent,
    HitsComponent,
    HitsCreateComponent,
    HitsDetailComponent,
    HitmenComponent,
    HitmenDetailComponent,
    HitsBulkComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    AppService, 
    HitsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
