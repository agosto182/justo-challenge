import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  readonly urlToRedirect = [
    '/',
    '/register'
  ]

  constructor(
    private appService: AppService,
    private router: Router
  ) {}

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if(this.appService.isLogged() && this.urlToRedirect.indexOf(event.url) > -1) {
          this.router.navigate(['/hits']);
        }
      }
    })
  }
}
