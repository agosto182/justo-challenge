import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  token: string | null = null;
  apiUrl = environment.apiUrl;
  user: any = {};

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.getSession();
  }

  getSession() {
    this.getLocalToken();
    this.getUser();
  }

  getLocalToken() {
    const localToken = localStorage.getItem('token');
    if(localToken) {
      this.token = localToken;
    }
  }
  
  getUser() {
    const userString = localStorage.getItem('user');
    if(userString){
      this.user = JSON.parse(userString);
    }
  }

  tryLogin(credentials: any) {
    return this.http.post(`${this.apiUrl}/auth/login`, credentials);
  }

  tryRegister(data: any) {
    return this.http.post(`${this.apiUrl}/auth/register`, data);
  }

  isLogged() {
    return this.token ? true : false;
  }

  logout() {
    localStorage.removeItem('token');
    this.token = null;
    this.user = {};
  }

  setAuth(data: any) {
    this.token = data.token;
    this.user = data.user;

    localStorage.setItem('token', data.token);
    localStorage.setItem('user', JSON.stringify(data.user));
  }

  isManager() {
    const isManager = this.user.userType == 1 || this.user.id == 1;
    return isManager;
  }
}
