import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  badRegister = false;
  messages: any = [];
  registerForm;

  constructor(
    private appService: AppService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.registerForm = this.formBuilder.group({
      email: '',
      name: '',
      password: ''
    });
  }

  submit(data: any) {
    this.appService.tryRegister(data).subscribe(
      (res: any) => {
        this.appService.setAuth(res);
        this.router.navigate(['hits']);
      },
      (err: any) => {
        console.warn(err);
        this.badRegister = true;
        this.messages = err.error.message;
      }
    );
  }
}
